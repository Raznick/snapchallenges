import os
import sys
import time
import json
import random
import logging
import argparse
import datetime
import requests
import traceback

from bs4 import BeautifulSoup

DATE_FORMAT = '%Y%m%d_%HM%%S'

CONSOLE_LOG_FORMAT = '%(levelname)-12s %(name)-20s %(message)s'
FILE_LOG_FORMAT = '%(asctime)-23s %(levelname)-20s %(name)-12s %(message)s'

class Crawler(object):
    _FILE_EXTENSIONS = ['.pdf', '.jpeg', '.jpg', '.png', '.mp3', '.mp4']
    _BANNED_WILDCARDS = ['imdb.com', 'old.reddit']

    def __init__(self, logger, max_depth, secured):
        self._logger = logger.getChild('crawl')
        self._max_depth = max(1, max_depth)
        self._prefix = 'https' if secured else 'http'
        self._history = {}
        self._session = requests.Session()
        self._session.headers['User-Agent'] = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.108 Safari/537.36'
        self._keywords = [
            {'nicks': [b'iron man', b'iron-man', b'ironman'], 'count': 0},
            {'nicks': [b'captain america', b'cpt. america', b'cpt america'], 'count': 0},
            {'nicks': [b'hulk'], 'count': 0},
            {'nicks': [b'thor'], 'count': 0},
            {'nicks': [b'thanos'], 'count': 0},
        ]

    def _has_already_crawled(self, url):
        real_url = url[url.find('//') + len('//'):]
        slash_index = real_url.find('/')
        if slash_index == -1:
            if real_url in self._history:
                return True
            self._history[real_url] = []
            return False
        page = real_url[slash_index + 1:]
        domain = real_url[:slash_index]
        if domain not in self._history:
            self._history[domain] = [page]
            return False
        if page in self._history[domain]:
            return True
        self._history[domain].append(page)
        return False

    def _is_banned_url(self, url):
        for banned in self._BANNED_WILDCARDS:
            if banned.lower() in url.lower():
                return True
        return False

    def _is_external_file(self, url):
        for ext in self._FILE_EXTENSIONS:
            if url.lower().endswith(ext):
                return True
        return False

    def _should_crawl(self, url, current_depth):
        if current_depth >= self._max_depth:
            return 'Max depth exceeded'
        if self._is_external_file(url):
            return 'This is an external file'
        if self._is_banned_url(url):
            return 'This is a banned URL'
        if self._has_already_crawled(url):
            return 'We\'ve already crawled this one'
        return None

    def crawl(self, url, current_depth=0):
        if url.endswith('/'):
            url = url[:-1]
        local_logger = self._logger.getChild(str(current_depth))

        reason = self._should_crawl(url, current_depth)
        if reason is not None:
            local_logger.warning('Not crawling "{0}": {1}'.format(url, reason))
            return
        
        local_logger.critical('Crawling: "{0}"'.format(url))
        time.sleep(round(random.uniform(0.1, 0.3), 2))
        try:
            response = self._session.get(url)
            if response.ok:
                edited_content = response.content.lower()
                for avenger in self._keywords:
                    for nickname in avenger['nicks']:
                        index = edited_content.find(nickname)
                        while index != -1:
                            avenger['count'] += 1
                            index = edited_content.find(nickname, index + 1)
                soup = BeautifulSoup(response.content, 'html.parser')
                for link in soup.findAll('a'):
                    href = link.get('href')
                    if href is not None and href.startswith(self._prefix):
                        self.crawl(href, current_depth + 1)
        except Exception as e:
            local_logger.error('Error while trying to crawl "{0}": {1}'.format(url, e))
            local_logger.error(traceback.format_exc())

    def close(self):
        self._session.close()

    def print_report(self):
        self._logger.critical('--- Report ---')
        for avenger in self._keywords:
            self._logger.critical('{0}: {1}'.format(avenger['nicks'][0], avenger['count']))


def setup_logging(log_file, verbose):
    logging_level = logging.INFO
    if verbose:
        logging_level = logging.DEBUG

    logger = logging.getLogger('main')
    logger.setLevel(logging_level)

    stream_handler = logging.StreamHandler(sys.stdout)
    stream_handler.setLevel(logging_level)
    stream_handler.setFormatter(logging.Formatter(CONSOLE_LOG_FORMAT))
    logger.addHandler(stream_handler)

    if log_file is not None:
        os.makedirs(os.path.dirname(log_file), exist_ok=True)
        file_handler = logging.FileHandler(log_file)
        file_handler.setLevel(logging_level)
        file_handler.setFormatter(logging.Formatter(FILE_LOG_FORMAT))
        logger.addHandler(file_handler)

    return logger

def generate_log_file_path(prefix, suffix):
    return os.path.join('.', 'products', '{0}_{1}.{2}'.format(prefix, datetime.datetime.now().strftime(DATE_FORMAT), suffix))

def parse_arguments():
    parser = argparse.ArgumentParser(description='SNAP\'s Web Crawler')
    parser.add_argument('-u', '--url', dest='url', type=str, required=True, help='Base URL to start with')
    parser.add_argument('-d', '--depth', dest='max_depth', type=int, default=1, help='Max depth to crawl (Default: 1)')
    parser.add_argument('-s', '--secured', dest='only_https', default=False, action='store_true', help='Only crawl HTTPS URLs (Default: False)')
    parser.add_argument('-v', '--verbose', dest='verbose', default=False, action='store_true', help='Log extended information (Default: False)')
    parser.add_argument('-l', '--log', dest='log_file', type=str, default=generate_log_file_path('crawl', 'log'), help='Save the session\'s output to a log file too (Default: Local \'products\' folder)')
    return parser.parse_args()

def main():
    args = parse_arguments()
    logger = setup_logging(args.log_file, args.verbose)

    start_time = time.perf_counter()
    logger.info('SNAP Crawler started!')
    logger.info('Input command line: "{0}"'.format(*sys.argv))

    crawler = Crawler(logger, args.max_depth, args.only_https)
    logger.info('Starting to crawl!')
    try:
        crawler.crawl(args.url)
    except Exception as e:
        logger.error('Error while crawling: {0}'.format(e))
        logger.error(traceback.format_exc())
    crawler.print_report()
    crawler.close()
    logger.info('SNAP crawler has finished in {0} seconds'.format(round(time.perf_counter() - start_time, 3)))


if __name__ == '__main__':
    main()

