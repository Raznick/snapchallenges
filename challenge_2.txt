Start date: 8/3/19 Whenever I upload the video
End date:   15/3/19 23:59
Send the github link through discord!
                             -------

Write a wise web crawler.
Read about this: https://en.wikipedia.org/wiki/Web_crawler
You must only use GET methods.
More features will win you more points.
Possible features:
- Parse only links -> Parse all links presented in the HTML like anchors, images, scripts.
- Support HTTPS
- Support Google's "robots.txt"
- Support search depth, and different kinds of crawls.
- Be careful not to get "banned" by certain sites!
- Think: How would you like to save the data? What data do you want to save?

Only allowed external modules: "requests", "httplib", "BeautifulSoup"
-----------------------------