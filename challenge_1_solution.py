#!/usr/bin/python3

import requests
from urllib.parse import urljoin

def sanitize_input(data):
    return data.lower().replace(' ', '-').replace('_', '-')

def parse_user_input(user_input):
    return user_input.split(' -> ')

def http_get_as_json(url):
    r = requests.get(url)
    if not r.ok:
        raise Exception('Bad URL, status code: {0}'.format(r.status_code))
    return r.json()

def api_caching(cache_type, base_page):
    def decorator(function):
        def wrapper(self, name):
            name = sanitize_input(name)
            if name not in self._cache[cache_type]:
                data = http_get_as_json(urljoin(base_page, name))
                function(self, name, data)
            return self._cache[cache_type][name]
        return wrapper
    return decorator

class MyPokeAPI(object):

    _TYPE_CACHE = 0
    _MOVE_CACHE = 1
    _POKEMON_CACHE = 2

    _BASE_URL = 'https://pokeapi.co/api/v2/'
    _TYPE_API = urljoin(_BASE_URL, 'type/')
    _MOVE_API = urljoin(_BASE_URL, 'move/')
    _POKEMON_API = urljoin(_BASE_URL, 'pokemon/')

    def __init__(self):
        self._cache = {
            self._TYPE_CACHE: {},
            self._MOVE_CACHE: {},
            self._POKEMON_CACHE: {}
        }

    @api_caching(_POKEMON_CACHE, _POKEMON_API)
    def _get_pokemon_data(self, name, data):
        self._cache[self._POKEMON_CACHE][name] = [t['type']['name'] for t in data['types']]
    
    @api_caching(_MOVE_CACHE, _MOVE_API)
    def _get_move_data(self, name, data):
        self._cache[self._MOVE_CACHE][name] = data['type']['name']

    @api_caching(_TYPE_CACHE, _TYPE_API)
    def _get_type_data(self, name, data):
        self._cache[self._TYPE_CACHE][name] = {}
        relation = data['damage_relations']
        for e in relation['double_damage_to']:
            self._cache[self._TYPE_CACHE][name][e['name']] = 2
        for e in relation['half_damage_to']:
            self._cache[self._TYPE_CACHE][name][e['name']] = 0.5
        for e in relation['no_damage_to']:
            self._cache[self._TYPE_CACHE][name][e['name']] = 0

    def calculate_damage_amplifier(self, move_name, pokemon_name):
        type_data = self._get_type_data(self._get_move_data(move_name))
        pokemon_data = self._get_pokemon_data(pokemon_name)
        amplifier = 1
        for t in pokemon_data:
            if t in type_data:
                amplifier *= type_data[t]
        return amplifier


def main():
    poke_api = MyPokeAPI()
    done = False
    while not done:
        try:
            data = input('Enter (Move -> Pokemon): ')
            if data == 'exit':
                done = True
                continue
            move, pokemon = parse_user_input(data)
            print('Amplifier: x{0}'.format(poke_api.calculate_damage_amplifier(move, pokemon)))
        except Exception as e:
            print('Exception: {0}'.format(e))


if __name__ == '__main__':
    main()